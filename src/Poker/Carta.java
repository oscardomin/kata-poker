package Poker;

/**
 * PartidaPoker in kata-poker project
 * Created by oscar on 19/02/14.
 */
public class Carta {
    private ValorCarta valor;
    private Palo palo;

    public Carta(ValorCarta valor, Palo palo) {
        this.valor = valor;
        this.palo = palo;
    }

    public ValorCarta getValor() {
        return valor;
    }

    public void setValor(ValorCarta valor) {
        this.valor = valor;
    }

    public Palo getPalo() {
        return palo;
    }

    public void setPalo(Palo palo) {
        this.palo = palo;
    }

    @Override
    public String toString() {
        return "Carta{" +
                "valor=" + valor +
                ", palo=" + palo +
                '}';
    }
}
