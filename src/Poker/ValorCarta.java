package Poker;

/**
 * PartidaPoker in kata-poker project
 * Created by oscar on 19/02/14.
 */
public enum ValorCarta {
    ACE("A",1),
    KING("K",13),
    QUEEN("Q",12),
    JACK("J",11),
    TEN("10",10),
    NINE("9",9),
    EIGHT("8",8),
    SEVEN("7",7),
    SIX("6",6),
    FIVE("5",5),
    FOUR("4",4),
    THREE("3",3),
    TWO("2",2);

    private String carta;
    private int valor;

    ValorCarta(String carta, int valor) {
        this.carta = carta;
        this.valor = valor;
    }

    public String getCarta() {
        return carta;
    }

    public int getValor() {
        return valor;
    }
}
