package Poker;

import java.lang.reflect.Array;

/**
 * PartidaPoker in kata-poker project
 * Created by oscar on 19/02/14.
 */
public class Baraja {
    private Carta cartas[] = new Carta[52];

    public Baraja(Carta[] cartas) {
        this.cartas = cartas;
    }

    public Baraja() {
        int i = 0;
        for(Palo p: Palo.values()) {
            for(ValorCarta v: ValorCarta.values()){
                cartas[i] = new Carta(v, p);
                i++;
            }
        }
    }

    public void printBaraja() {
        for(int i = 0; i<52; i++){
            System.out.println(cartas[i].getValor().getCarta() + " de " + cartas[i].getPalo().getPalo());
        }
    }

    public Carta[] getCartas() {
        return cartas;
    }

    public void setCartas(Carta[] cartas) {
        this.cartas = cartas;
    }

    public Carta getRandomCard() {
        int randomNum = (int)(Math.random()*52);
        return this.cartas[randomNum];
    }
}
