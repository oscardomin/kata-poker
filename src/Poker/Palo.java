package Poker;

/**
 * PartidaPoker in kata-poker project
 * Created by oscar on 19/02/14.
 */
public enum Palo {
    PICAS(0,"Picas"),
    CORAZONES(1,"Corazones"),
    ROMBOS(2,"Rombos"),
    TRÉBOLES(3,"Treboles");

    ;
    private int valor;
    private String palo;

    Palo(int valor, String palo) {
        this.valor = valor;
        this.palo = palo;
    }

    public int getValor() {
        return valor;
    }

    public String getPalo() {
        return palo;
    }
}
