package Poker;

import java.util.Arrays;

/**
 * PartidaPoker in kata-poker project
 * Created by oscar on 19/02/14.
 */
public class Mano {
    private Carta mano[] = new Carta[5];

    public Mano(Carta[] mano) {
        this.mano = mano;
    }

    public Mano(Baraja baraja) {
        for(int i = 0; i < mano.length; i++) {
            mano[i] = baraja.getRandomCard();
        }
    }

    public Carta[] getMano() {
        return mano;
    }

    public void setMano(Carta[] mano) {
        this.mano = mano;
    }

    @Override
    public String toString() {
        return "Mano{" +
                "mano=" + Arrays.toString(mano) +
                '}';
    }

    public void ordenarMano() {
        //Inicialitzacio RANDOM, s'acabara substituint

        for(int i = 0; i<getMano().length; i++) {
            int minimo = i;
            for(int j = i+1; j<getMano().length; j++) {
                if(mano[j].getValor().getValor() < mano[minimo].getValor().getValor()) minimo = j;
            }

            Carta aux = new Carta(mano[i].getValor(), mano[i].getPalo());

            mano[i].setValor(mano[minimo].getValor());
            mano[i].setPalo(mano[minimo].getPalo());

            mano[minimo].setValor(aux.getValor());
            mano[minimo].setPalo(aux.getPalo());
        }
    }

    public boolean hayEscaleraReal() {
        ordenarMano();
        return mano[0].getValor().getValor() == 1 &&
                 mano[1].getValor().getValor() == 10 &&
                 mano[2].getValor().getValor() == 11 &&
                 mano[3].getValor().getValor() == 12 &&
                 mano[4].getValor().getValor() == 13;
    }

    public boolean hayRepoker() {
        return (mano[0].getValor().getValor() == mano[1].getValor().getValor() &&
                mano[0].getValor().getValor() == mano[2].getValor().getValor()&&
                mano[0].getValor().getValor() == mano[3].getValor().getValor() &&
                mano[0].getValor().getValor() == mano[4].getValor().getValor());
    }

    public boolean hayPoker() {
        ordenarMano();
        return (mano[0].getValor().getValor() == mano[1].getValor().getValor() &&
                mano[0].getValor().getValor() == mano[2].getValor().getValor() &&
                mano[0].getValor().getValor() == mano[3].getValor().getValor()) ||
                (mano[1].getValor().getValor() == mano[2].getValor().getValor() &&
                mano[1].getValor().getValor() == mano[3].getValor().getValor() &&
                mano[1].getValor().getValor() == mano[4].getValor().getValor());
    }

    public boolean hayFull() {
        ordenarMano();
        if(mano[0].getValor().getValor() == mano[1].getValor().getValor()) {
            if(mano[0].getValor().getValor() == mano[2].getValor().getValor())
                return mano[3].getValor().getValor() == mano[4].getValor().getValor();
            else return mano[2].getValor().getValor() == mano[3].getValor().getValor() &&
                        mano[2].getValor().getValor() == mano[4].getValor().getValor();
        }
        else return false;
    }

    public boolean hayColor() {
        return getMano()[0].getPalo().getValor() == getMano()[1].getPalo().getValor() &&
                getMano()[0].getPalo().getValor() == getMano()[2].getPalo().getValor() &&
                getMano()[0].getPalo().getValor() == getMano()[3].getPalo().getValor() &&
                getMano()[0].getPalo().getValor() == getMano()[4].getPalo().getValor();
    }

    public boolean hayEscalera() {
        ordenarMano();
        return mano[0].getValor().getValor() + 1 == mano[1].getValor().getValor() &&
                mano[1].getValor().getValor() + 1 == mano[2].getValor().getValor() &&
                mano[2].getValor().getValor() + 1 == mano[3].getValor().getValor() &&
                mano[3].getValor().getValor() + 1 == mano[4].getValor().getValor();
    }

    public boolean hayTrio() {
        ordenarMano();
        return (mano[0] == mano[1] && mano[0] == mano[2]) ||
        (mano[1] == mano[2] && mano[1] == mano[3]) ||
        (mano[2] == mano[3] && mano[2] == mano[4]);
    }

    public boolean hayDoblePareja() {
        ordenarMano();
        if(mano[0].getValor().getValor() == mano[1].getValor().getValor())
            return mano[2].getValor().getValor() == mano[3].getValor().getValor()
                    || mano[3].getValor().getValor() == mano[4].getValor().getValor();
        else
            return mano[1].getValor().getValor() == mano[2].getValor().getValor()
                && mano[3].getValor().getValor() == mano[4].getValor().getValor();
    }

    public boolean hayPareja() {
        boolean hayPareja = false;
        for(int j = 0; j < mano.length-1; j++){
            for(int i = j+1; i < mano.length && !hayPareja; i++) {
                if(mano[j].getValor().getValor() == mano[i].getValor().getValor()) hayPareja = true;
            }
        }
        return hayPareja;
    }

    public int numParejas() {
        int numParejas = 0;
        for(int j = 0; j < mano.length-1; j++){
            for(int i = j+1; i < mano.length; i++) {
                if(mano[j].getValor().getValor() == mano[i].getValor().getValor()) numParejas++;
            }
        }
        return numParejas;
    }

}
