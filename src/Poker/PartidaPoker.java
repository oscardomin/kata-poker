package Poker;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: oscar
 * Date: 29/10/13
 * Time: 2:01
 */


public class PartidaPoker {

    public static void main(String[] args) throws IOException {
        Baraja bj = new Baraja();
        //bj.printBaraja();

        Mano mano = new Mano(bj);

        mano.getMano()[0] = new Carta(ValorCarta.KING,Palo.CORAZONES);
        mano.getMano()[1] = new Carta(ValorCarta.KING,Palo.CORAZONES);
        mano.getMano()[2] = new Carta(ValorCarta.ACE,Palo.PICAS);
        mano.getMano()[3] = new Carta(ValorCarta.KING,Palo.CORAZONES);
        mano.getMano()[4] = new Carta(ValorCarta.ACE,Palo.CORAZONES);

        mano.ordenarMano();
        System.out.println(mano.toString());

        if(mano.hayEscaleraReal() && mano.hayColor()) System.out.println("ESCALERA REAL DE COLOR!");
        else if(mano.hayEscalera() && mano.hayColor()) System.out.println("ESCALERA DE COLOR!");
        else if(mano.hayRepoker()) System.out.println("REPOKER!");
        else if(mano.hayPoker()) System.out.println("POKER!");
        else if(mano.hayFull()) System.out.println("FULL!");
        else if(mano.hayColor()) System.out.println("COLOR!");
        else if(mano.hayEscalera()) System.out.println("ESCALERA!");
        else if(mano.hayTrio()) System.out.println("TRIO!");
        else if(mano.hayDoblePareja()) System.out.println("DOBLE PAREJA!");
        else if(mano.hayPareja()) System.out.println("PAREJA!");
    }

}