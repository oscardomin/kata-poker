# Kata-Poker Repository

##Historia:
Los inicios de este juego ronda el año 1829 donde se jugaba con una baraja de sólo 20 cartas. La versión más extendida de poker es la Texas Hold’em. En este poker existen 9 combinaciones de cartas ganadoras, cada una mejor que la anterior. 

##Datos:
La baraja de cartas está compuesta por 52 cartas en 4 palos: picas, tréboles, corazones y diamantes. Ignoraremos el uso de comodines para esta kata.

##Objetivo:
Debes escribir un programa capaz de identificar el nombre de una combinación de cartas ganadora. 

###Entrada:
La entrada al programa deberán ser las 5 cartas de una jugada y el resultado el nombre de la combinación ganadora. 

###Reglas:
Las diferentes combinaciones ganadoras son:

 * Escalera Real de Color: La mejor jugada del poker. Comprende las cartas 10, J, Q, K y A del mismo palo. Todo jugador tiene derecho a una de éstas a lo largo de su vida.
 * Escalera de Color: Cinco cartas de orden consecutivo del mismo palo. Cuanto más alta sea la carta más alta de la escalera, mejor es el ranking de la mano. 
 * Poker: Combinación de cuatro cartas del mismo número. Es la jugada más característica del juego. 
 * Full: Es la combinación de tres cartas de un mismo valor y dos cartas de otro mismo valor. 
 * Color: Son cinco cartas del mismo palo, sin tomar en cuenta la secuencia. 
 * Escalera: Cinco cartas de distinto palo donde importa la secuencia. 
 * Trío: Lo único que se necesita para este tipo de jugadas es tres cartas del mismo valor. 
 * Dobles: Parejas Requiere dos grupos de dos cartas del mismo valor. 
 * Una Pareja: Solamente se necesita una combinación de dos cartas de igual valor. 

###Ampliación:
Si quieres añadirme más emoción a la kata añade la siguiente regla: La baraja contendrá 2 comodines que podrán valer por cualquier otra carta de la baraja. Adapta tu programa para soportar comodines.